const express = require('express');
const mysql = require('mysql');
const cors = require('cors');

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const crypto = require('crypto');
const bcrypt = require('bcrypt');

const saltRounds = 10;
const app = express();

app.use(express.json());
app.use(
  cors({
    origin: ['http://localhost:3000'],
    methods: ['GET', 'POST'],
    credentials: true
  })
);
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(
  session({
    key: 'userId',
    secret: 'subscribe',
    resave: false,
    saveUninitialized: false,
    cookie: {
      expires: 60 * 60 * 24
    }
  })
);

const db = mysql.createConnection({
  user: 'root',
  host: '127.0.0.1',
  password: 'password',
  database: 'LoginSystem',
  port: '3306'
});

app.post('/register', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  bcrypt.hash(password, saltRounds, (err, hash) => {
    if (err) {
      console.log(err);
    }

    db.query(
      'INSERT INTO users (username, password) VALUES (?,?)',
      [username, hash],
      (err, result) => {
        console.log(err);
      }
    );
  });
});

app.get('/login', (req, res) => {
  if (req.session.user) {
    res.send({ loggedIn: true, user: req.session.user });
  } else {
    res.send({ loggedIn: false });
  }
});

app.post('/login', (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  db.query(
    'SELECT * FROM users WHERE username = ?;',
    username,
    (err, result) => {
      if (err) {
        res.send({ err: err });
      }

      if (result.length > 0) {
        const hasher = crypto.createHmac('sha256', result[0].salt);

        if (hasher.update(password).digest('hex') === result[0].password) {
          req.session.user = result;
          res.send(result);
        } else {
          res.send({ message: 'Wrong username/password combination!' });
        }
      } else {
        res.send({ message: 'User doesn\'t exist' });
      }
    }
  );
});

app.post('/import-user', (req, res) => {
  const userList = req.body.users;

  userList.forEach((element) => db.query(
    'INSERT INTO users (username, salt, password, firstName, lastName, city, country, postalCode, longitude, latitude, email, dateOfBirth, phone, nat, PICTURE, clearPassword, gender, title, cell, registeredDate, registeredAge, dobAge) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
    [
      element.name.first, element.login.salt,
      crypto.createHmac('sha256', element.login.salt).update(element.login.password).digest('hex'),
      element.name.first, element.name.last,
      element.location.city,
      element.location.country,
      element.location.postcode,
      element.location.coordinates.longitude,
      element.location.coordinates.latitude,
      element.email, element.dob.date,
      element.phone, element.nat,
      element.picture.medium,
      element.login.password,
      element.gender,
      element.name.title,
      element.cell,
      element.registered.date,
      element.registered.age,
      element.dob.age
    ],
    (err, result) => {
      console.log(err);
    }
  ));
});

function dbQuery(databaseQuery) {
  return new Promise((data) => {
    db.query(databaseQuery, (error, result) => { // change db->connection for your code
      if (error) {
        console.log(error);
        throw error;
      }
      try {
        data(JSON.parse(JSON.stringify(result)));
      } catch (error) {
        data({});
        throw error;
      }
    });
  });
}

app.post('/utilisateur', async (req, res) => {
  const finalArrayStats = {};
  const idUser = req.body.id;
  const request = `SELECT * FROM users WHERE users.id = ${idUser.id}`;

  finalArrayStats.userSearch = await dbQuery(request);

  res.send(finalArrayStats);
});

app.post('/recherche', async (req, res) => {
  const finalArrayStats = {};
  const searchInput = req.body.searchInput;
  const type = req.body.type;
  const request = `SELECT * FROM users WHERE users.${type} LIKE '%${searchInput}%'`;

  finalArrayStats.responseToSearch = await dbQuery(request);
  res.send(finalArrayStats);
});

app.get('/dashboard', async (req, res) => {
  const finalArrayStats = {};

  const resultUser = await dbQuery('SELECT COUNT(*) as nbUser FROM users;');
  finalArrayStats.nbUser = resultUser[0].nbUser;

  const resultGender = await dbQuery('SELECT gender, COUNT(*) as nbGender FROM users GROUP BY users.gender');
  finalArrayStats.nbFemale = resultGender[1].nbGender;
  finalArrayStats.nbMale = resultGender[0].nbGender;

  const resultCountry = await dbQuery('SELECT country, COUNT(*) as nbUserCountry FROM users GROUP BY users.country');
  finalArrayStats.nbByCountry = resultCountry;

  const resultCoordinate = await dbQuery('SELECT firstName, lastName, id, latitude, longitude FROM users LIMIT 100');
  finalArrayStats.coordinate = resultCoordinate;

  res.send(finalArrayStats);
});

app.listen(3001, () => {
  console.log('running server');
});

// Start project on specific port npm start -- --port ####
// start mysql server mysql.server start
