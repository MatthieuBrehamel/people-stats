export const actionTypes = {
  DESCRIPTION_HELLO: 'DESCRIPTION_HELLO'
};

export const descriptionAction = () => ({
  type: actionTypes.DESCRIPTION_HELLO,
  payload: {
    description: 'description'
  }
});
