import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
import Axios from 'axios';
import './index.scss';
import {
  Chart as ChartJS, ArcElement, Tooltip, Legend
} from 'chart.js';
import { Doughnut } from 'react-chartjs-2';
import ReactMapGL, { Marker } from 'react-map-gl';
import { Navbar } from 'react-bootstrap';

ChartJS.register(ArcElement, Tooltip, Legend);

// eslint-disable-next-line consistent-return
const Home = () => {
  const cookies = new Cookies();
  // eslint-disable-next-line no-unused-vars
  const [dateState, useDateState] = useState(new Date());
  const [nbUserStat, setNbUserStat] = useState('');
  const [nbGender, setGender] = useState([]);
  const [coordinate, setCoordinate] = useState([]);
  const [countryName, setCountryName] = useState([]);
  const [countryNbUser, setCountryNbUser] = useState([]);
  const [viewport, setViewport] = useState({
    latitude: 46.71109,
    longitude: 1.7191036,
    zoom: 1,
    width: window.innerWidth,
    height: window.innerHeight
  });

  const arrayCountry = [];
  const arrayUserCountry = [];

  useEffect(() => {
    const test = async () => {
      Axios.get('http://localhost:3001/dashboard').then((response) => {
        if (response.data.nbUser) {
          setNbUserStat(response.data.nbUser);
        }

        if (response.data.nbFemale && response.data.nbMale) {
          // eslint-disable-next-line max-len
          let percentFemale = (response.data.nbFemale / (response.data.nbFemale + response.data.nbMale)) * 100;
          let percentMale = 100 - percentFemale;

          percentMale = Math.round(percentMale * 100) / 100;
          percentFemale = Math.round(percentFemale * 100) / 100;
          setGender([percentMale, percentFemale]);
        }

        if (response.data.nbByCountry) {
          // eslint-disable-next-line array-callback-return
          response.data.nbByCountry.map((data) => {
            arrayCountry.push(data.country);
            arrayUserCountry.push(data.nbUserCountry);
          });

          setCountryName(arrayCountry);
          setCountryNbUser(arrayUserCountry);
        }

        if (response.data.coordinate) {
          setCoordinate(response.data.coordinate);
        }
      });
    };

    test();
  }, []);

  if (cookies.get('isLogged')) {
    const user = cookies.get('userData');
    // const handleOnClick = () => {
    //   dispatch(descriptionAction());
    // };

    const data = {
      labels: ['Femmes', 'Hommes'],
      datasets: [
        {
          label: '% of gender',
          data: [nbGender[1], nbGender[0]],
          backgroundColor: [
            'rgba(255, 99, 132, 0.7)',
            'rgba(54, 162, 235, 0.7)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)'
          ],
          borderWidth: 1
        }
      ]
    };

    const dataCountry = {
      labels: countryName,
      datasets: [
        {
          label: '% of gender',
          data: countryNbUser,
          backgroundColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)',
            'rgba(110, 114, 20, 1)',
            'rgba(118, 183, 172, 1)',
            'rgba(0, 148, 97, 1)',
            'rgba(129, 78, 40, 1)',
            'rgba(129, 199, 111, 1)'
          ],
          borderColor: [
            'rgba(255, 99, 132, 1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)',
            'rgba(110, 114, 20, 1)',
            'rgba(118, 183, 172, 1)',
            'rgba(0, 148, 97, 1)',
            'rgba(129, 78, 40, 1)',
            'rgba(129, 199, 111, 1)'
          ],
          borderWidth: 1
        }
      ]
    };

    return (
      <>
        <div>
          <Navbar className="navbar">
            <div className="d-flex flex-row justify-content-between w-100">
              <div className="m-2 d-flex align-items-center">
                Nous sommes le : &nbsp;
                {' '}
                {dateState.toLocaleDateString('fr-FR', {
                  day: 'numeric',
                  month: 'long',
                  year: 'numeric'
                })}&nbsp;
                Il est : &nbsp;
                {dateState.toLocaleString('fr-FR', {
                  hour: 'numeric',
                  minute: 'numeric',
                  hour12: false
                })}&nbsp;
              </div>
              <div className="m-2">
                Bonjour&nbsp;
                {user.lastName}&nbsp;
                {user.firstName}&nbsp;
                {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
                <img src={user.PICTURE} alt="profile picture" className="picture"/>
              </div>
            </div>
          </Navbar>
          <div className="m-4">
            Il y a actuellement <strong>{nbUserStat}</strong> utilisateurs inscrits.
          </div>
          <div style={{
            height: 300, display: 'flex', direction: 'column', justifyContent: 'center', marginBottom: 40
          }}>
            <div style={{ width: 300, height: 300 }}>
              <Doughnut data={data} width={100}/>
            </div>
            <div style={{ width: 300, height: 300 }}>
              <Doughnut data={dataCountry} width={100}/>
            </div>
          </div>
          <table className="table table-bordered table-active" style={{
            display: 'flex', direction: 'row', justifyContent: 'center'
          }}>
            <div>
              <tr>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>id</th>
                <th>prénom</th>
                <th>nom</th>
              </tr>
              {
                coordinate.map((info, key) => <tr key={key}>
                  <td>{info.latitude}</td>
                  <td>{info.longitude}</td>
                  <td>{info.id}</td>
                  <td>{info.firstName}</td>
                  <td>{info.lastName}</td>
                </tr>)
              }
            </div>
          </table>
        </div>
        <div style={{ width: window.innerWidth, height: 800 }}>
          <ReactMapGL
            {...viewport}
            onViewportChange={(nextViewport) => { setViewport(nextViewport); }}
            mapStyle="mapbox://styles/mapbox/streets-v9"
            mapboxAccessToken={'pk.eyJ1IjoibWFnbWFiaWpvdXgiLCJhIjoiY2wxYjJvNDBqMGN0bDNpcDhpMGljMHg2ZiJ9.cMKU9cImzIumsOpOuhgE-g'}
          >
            <Marker longitude={-122.4376} latitude={37.7577}>
              <div style={{ color: 'white' }}>You are here</div>
            </Marker>
          </ReactMapGL>
        </div>
      </>
    );
  }
};

const mapStateToProps = (state) => ({
  description: state.home.description || 'hello'
});

export default connect(mapStateToProps)(Home);
