import React, { useEffect, useState } from 'react';
import Axios from 'axios';
import Cookies from 'universal-cookie';
import { Navigate } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import './index.scss';

const LoginComponent = () => {
  const cookies2 = new Cookies();
  // const [usernameReg, setUsernameReg] = useState('');
  // const [passwordReg, setPasswordReg] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loginStatus, setLoginStatus] = useState('');

  Axios.defaults.withCredentials = true;

  // const register = () => {
  //   Axios.post('http://localhost:3001/register', {
  //     username: usernameReg,
  //     password: passwordReg
  //   }).then((response) => {
  //     console.log(response);
  //   });
  // };

  const importUser = () => {
    Axios.defaults.withCredentials = false;
    Axios.get('https://randomuser.me/api?results=70').then((response) => {
      Axios.post('http://localhost:3001/import-user', {
        users: response.data.results
      }).then((response) => {
        console.log(response);
      });
    });
  };

  const login = () => {
    Axios.post('http://localhost:3001/login', {
      username,
      password
    }).then((response) => {
      const cookies = new Cookies();
      cookies.set('isLogged', true, { path: '/' });
      cookies.set('userData', response.data[0], { path: '/' });
      if (response.data.message) {
        setLoginStatus(response.data.message);
      } else {
        setLoginStatus(`${response.data[0].firstName} ${response.data[0].lastName}`);
      }
    });
  };

  useEffect(() => {
    // eslint-disable-next-line consistent-return
    Axios.get('http://localhost:3001/login').then((response) => {
      if (response.data.loggedIn === true) {
        setLoginStatus(`${response.data[0].firstName} ${response.data[0].lastName}`);
        return (
          <Navigate to="/dashboard" replace={true}/>
        );
      }
    });
  }, []);

  if (cookies2.get('isLogged') && cookies2.get('userData') !== 'undefined') {
    return (
      <Navigate to="/dashboard" replace={true}/>
    );
  }

  return (
    <div className='d-flex flex-column justify-content-center align-content-center text-center'>
      {/* <div className='registration'> */}
      {/*  <h1>Registration</h1> */}
      {/*  <h2>Username</h2> */}
      {/*  <input */}
      {/*    type='text' */}
      {/*    onChange={ (e) => { */}
      {/*      setUsernameReg(e.target.value); */}
      {/*    }} */}
      {/*  /> */}
      {/*  <h2>Password</h2> */ }
      { /*  <input */ }
      { /*    type='text' */ }
      { /*    onChange={ (e) => { */ }
      { /*      setPasswordReg(e.target.value); */ }
      { /*    }} */ }
      { /*  /> */ }
      { /*  <button onClick={ register }> Register </button> */ }
      { /* </div> */ }

      <div className='login d-flex flex-column justify-content-center align-content-center'>
        <div className="m-2">
          <h1>Login</h1>
        </div>
        <div className="m-2">
          <input
            type='text'
            placeholder='Username...'
            onChange={(e) => {
              setUsername(e.target.value);
            }}
          />
          <input
            type='password'
            placeholder='Password...'
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          />
        </div>
        <div className="m-2">
          <Button onClick={login} className="mr-button btn-primary"> Login </Button>
          <Button onClick={importUser} className="btn-secondary"> Import User </Button>
        </div>
      </div>

      <h1>Hello {loginStatus} !</h1>
    </div>
  );
};

export default LoginComponent;
