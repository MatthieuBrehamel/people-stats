import React, { useEffect, useState } from 'react';
import Cookies from 'universal-cookie';
import Axios from 'axios';
import { Button, Navbar } from 'react-bootstrap';

// eslint-disable-next-line consistent-return
const SearchPage = () => {
  const cookies = new Cookies();
  // eslint-disable-next-line no-unused-vars
  const [dateState, useDateState] = useState(new Date());
  const [searchInput, setSearchInput] = useState('');
  const [selectedOption, setSelectedOption] = useState('');
  const [searchResult, setSearchResult] = useState([]);

  const search = async () => {
    Axios.post('http://localhost:3001/recherche', {
      searchInput,
      type: selectedOption
    }).then((response) => {
      setSearchResult(response.data.responseToSearch);
    });
  };

  useEffect(() => {
    const test = async () => {
      setSelectedOption('firstName');
    };

    test();
  }, []);

  const handleChange = (e) => {
    setSelectedOption(e.target.value);
  };

  if (cookies.get('isLogged')) {
    const user = cookies.get('userData');

    return (
      <>
        <div>
          <Navbar className="navbar">
            <div className="d-flex flex-row justify-content-between w-100">
              <div className="m-2 d-flex align-items-center">
                Nous sommes le : &nbsp;
                {' '}
                {dateState.toLocaleDateString('fr-FR', {
                  day: 'numeric',
                  month: 'long',
                  year: 'numeric'
                })}&nbsp;
                Il est : &nbsp;
                {dateState.toLocaleString('fr-FR', {
                  hour: 'numeric',
                  minute: 'numeric',
                  hour12: false
                })}&nbsp;
              </div>
              <div className="m-2">
                Bonjour&nbsp;
                {user.lastName}&nbsp;
                {user.firstName}&nbsp;
                {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
                <img src={user.PICTURE} alt="user picture" className="picture"/>
              </div>
            </div>
          </Navbar>
          <div className='m-3'>
            <div className='search'>
              <h1>Recherche utilisateur(s) : </h1>
              <div style={{ display: 'flex', direction: 'row', justifyContent: 'center' }}>
                <select style={{ marginRight: 20 }} onChange={handleChange} >
                  <option value="gender">Genre</option>
                  <option value="city">Ville</option>
                  <option selected value="phone">Téléphone</option>
                  <option selected value="firstName">Prénom</option>
                  <option value="lastName">Nom</option>
                </select>
                <input
                  type='text'
                  onChange={(e) => {
                    setSearchInput(e.target.value);
                  }}
                />
                <Button onClick={search} className="m-2"> Rechercher </Button>
              </div>
            </div>
          </div>
          <table className="table table-bordered m-5" style={{
            display: 'flex', direction: 'row', justifyContent: 'center'
          }}>
            <div>
              <tr>
                <th>Genre</th>
                <th>Titre</th>
                <th>Prénom</th>
                <th>Nom</th>
                <th>email</th>
                <th>photo</th>
                <th>téléphone</th>
                <th>fixe</th>
              </tr>
              {
                // eslint-disable-next-line react/jsx-key
                searchResult.map((info) => <tr>
                  <td>{info.gender}</td>
                  <td>{info.title}</td>
                  <td>{info.firstName}</td>
                  <td>{info.lastName}</td>
                  <td>{info.email}</td>
                  {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
                  <td><img src={info.PICTURE} alt="user search picture"/></td>
                  <td>{info.phone}</td>
                  <td>{info.cell}</td>
                </tr>)
              }
            </div>
          </table>
        </div>
      </>
    );
  }
};

export default SearchPage;
