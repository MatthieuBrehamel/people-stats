import React, { useEffect, useState } from 'react';
import Cookies from 'universal-cookie';
import { useParams } from 'react-router-dom';
import Axios from 'axios';
import { Navbar } from 'react-bootstrap';

// eslint-disable-next-line consistent-return
const UserPage = () => {
  const cookies = new Cookies();
  // eslint-disable-next-line no-unused-vars
  const [dateState, useDateState] = useState(new Date());
  const [userSearch, setUserSearch] = useState([]);
  const idUser = useParams();

  useEffect(() => {
    const test = async () => {
      Axios.post('http://localhost:3001/utilisateur', {
        id: idUser
      }).then((response) => {
        if (response.data.userSearch) {
          setUserSearch(response.data.userSearch);
        }
      });
    };

    test();
  }, []);

  if (cookies.get('isLogged')) {
    const user = cookies.get('userData');

    return (
      <>
        <div>
          <Navbar className="navbar">
            <div className="d-flex flex-row justify-content-between w-100">
              <div className="m-2 d-flex align-items-center">
                Nous sommes le : &nbsp;
                {' '}
                {dateState.toLocaleDateString('fr-FR', {
                  day: 'numeric',
                  month: 'long',
                  year: 'numeric'
                })}&nbsp;
                Il est : &nbsp;
                {dateState.toLocaleString('fr-FR', {
                  hour: 'numeric',
                  minute: 'numeric',
                  hour12: false
                })}&nbsp;
              </div>
              <div className="m-2">
                Bonjour&nbsp;
                {user.lastName}&nbsp;
                {user.firstName}&nbsp;
                {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
                <img src={user.PICTURE} alt="user picture" className="picture"/>
              </div>
            </div>
          </Navbar>
          <table className="table table-bordered m-5" style={{
            display: 'flex', direction: 'row', justifyContent: 'center'
          }}>
            <div>
              <tr>
                <th>Sexe</th>
                <th>Titre</th>
                <th>Prénom</th>
                <th>Nom</th>
                <th>email</th>
                <th>photo</th>
                <th>téléphone</th>
                <th>fixe</th>
                <th>Date d&apos;inscription</th>
                <th>Âge d&apos;inscription</th>
                <th>Date de naissance</th>
                <th>Âge</th>
              </tr>
              {
                userSearch.map((info, index) => <tr key={index}>
                  <td>{info.gender}</td>
                  <td>{info.title}</td>
                  <td>{info.firstName}</td>
                  <td>{info.lastName}</td>
                  <td>{info.email}</td>
                  {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
                  <td><img src={info.PICTURE} alt="user search picture"/></td>
                  <td>{info.phone}</td>
                  <td>{info.cell}</td>
                  <td>{info.registeredDate}</td>
                  <td>{info.registeredAge}</td>
                  <td>{info.dateOfBirth}</td>
                  <td>{info.dobAge}</td>
                </tr>)
              }
            </div>
          </table>
        </div>
      </>
    );
  }
};

export default UserPage;
