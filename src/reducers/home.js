import { actionTypes } from '../actions/home';

const initalState = {};

const home = (state = initalState, action) => {
  switch (action.type) {
    case actionTypes.DESCRIPTION_HELLO: {
      const { description } = action.payload;

      return {
        ...state,
        ...{ description }
      };
    }
    default: {
      return state;
    }
  }
};

export default home;
