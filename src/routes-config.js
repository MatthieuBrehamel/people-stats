import React from 'react';
import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom';

import DashBoard from './components/home/index';
import LoginComponent from './components/login/index';
import UserPage from './components/user/index';
import SearchPage from './components/search/index';
import NotFoundPage from './components/not-found/index';

const RoutesConfig = () => (
  <BrowserRouter>
    <Routes>
      <Route path='*' element={<NotFoundPage />} />
      <Route path="/" element={<LoginComponent />} />
      <Route path="/dashboard" element={<DashBoard />} />
      <Route path="/utilisateur/:id" exact element={<UserPage />} />
      <Route path="/recherche" exact element={<SearchPage />} />
    </Routes>
  </BrowserRouter>
);

export default RoutesConfig;
